module.exports.starredEntries = [
	{
		url: "https://example.com",
		title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
		hostname: "example.com",
	},
	{
		url: "https://example.com",
		title: "Donec mattis quam tortor, non mollis augue euismod",
		hostname: "example.com",
	},
	{
		url: "https://donec-mattis-quam-tortor.com",
		title: "Proin eros odio",
		hostname: "donec-mattis-quam-tortor.com",
	},
	{
		url: "https://example.com",
		title: "Donec non mollis augue euismod",
		hostname: "example.com",
		images: {
			original_url: "https://images.unsplash.com/photo-1532365673558-f9bb768644e7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=300&q=60"
		}
	},
	{
		url: "https://example.com",
		title: "Donec mattis quam tortor, non mollis augue euismod",
		hostname: "example.com",
	},
	{
		url: "https://example.com",
		title: "Consectetur adipiscing elit",
		hostname: "example.com",
	},
];
