const CacheAsset = require( "@11ty/eleventy-cache-assets" );
const https = require( "https" );
const Jimp = require( "jimp" );
const path = require ("path" );
const url = require( "url" );
const {starredEntries} = require( "./dev/allStars-entries" );

module.exports = async () =>
{
	let entries = await getStarredEntries( 6 );

	let promises = entries.map( async (entry) =>
	{
		let imageUrl;
		
		if( entry.images )
		{
			imageUrl = entry.images.original_url;
		}
		else
		{
			imageUrl = `https://generative-placeholders.glitch.me/image?width=600&height=300&style=tiles&id=${entry.id}`;
		}

		let imageFilename = await fetchImage( imageUrl, `./dist/images/all-stars`, entry.id );
		entry.imageUrl = `/images/all-stars/${path.basename( imageFilename )}`;
		
		return entry;
	});

	return Promise.all( promises )
		.then( () =>
		{
			return entries	
		});
};

/**
 * @param {number} count
 * @return {Promise<[number]>}
 */
function getStarredEntries( count=6 )
{
	if( process.env.NODE_ENV !== "production" && !process.env.FEEDBIN_LIVE )
	{
		console.warn( "⚠️ Using development data for All-Stars" );
		return Promise.resolve( starredEntries );
	}

	return getFeedbinEndpoint( "entries", { starred: true, mode: "extended" } )
		.then( entries =>
		{
			return entries
				.sort( (a, b) => a.id < b.id ? 1 : -1 )
				.slice( 0, count )
				.map( entry =>
				{
					entry.hostname = url.parse( entry.url )
						.hostname.replace( /^www\./, "" );
					return entry;
				});
		});
}

/**
 * @param {string} imageUrl
 * @param {string} dirname - Directory in which to save processed image
 * @param {string} basename
 */
async function fetchImage( imageUrl, dirname, basename )
{
	let buffer = await CacheAsset( imageUrl, {
		duration: "7d",
		type: "buffer"
	});

	let filename;

	return Jimp.read( buffer )
		.then( image =>
		{
			filename = `${dirname}/${basename}.${image.getExtension()}`;
			return image.write( filename );
		})
		.then( () =>
		{
			return filename;
		})
		.catch( error =>
		{
			console.error( error );
		});
}

/**
 * @param {string} endpoint - ex., "entries
 * @param {Object} params
 */
function getFeedbinEndpoint( endpoint, params={} )
{
	["FEEDBIN_EMAIL", "FEEDBIN_PASSWORD"].forEach( varName =>
	{
		if( !process.env[varName] )
		{
			throw new Error( `❌ Missing ${varName} environment variable` );
		}
	});

	return new Promise( (resolve, reject) =>
	{
		let options = {
			auth: `${process.env.FEEDBIN_EMAIL}:${process.env.FEEDBIN_PASSWORD}`,
		};

		let paramString = Object.keys( params )
			.map( key => `${key}=${params[key]}` )
			.join( "&" );

		let url = `https://api.feedbin.com/v2/${endpoint}.json?${paramString}`;

		https.get( url, options, response =>
		{
			let data = "";
			response.on( "data", chunk => data += chunk );
			response.on( "end", () =>
			{
				try
				{
					let json = JSON.parse( data.toString() )
					resolve( json );
				}
				catch( error )
				{
					reject( error );
				}
			});
		})
			.on( "error", error =>
			{
				reject( error );
			});
	});
}

module.exports.getStarredEntries = getStarredEntries;
