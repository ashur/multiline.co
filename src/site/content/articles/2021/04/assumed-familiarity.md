---
title: Assumed Familiarity
date: 2021-04-19T17:57:19.575Z
category: Writing
---
In one of those exciting moments of chance—when you spot a pattern on the verge of emerging (or, at least, one that's new to you)—I recently came across a pair of banners by two different authors, each setting the stage for their respective posts.

First, a note from Stephanie Eckles leading into her article “[Use Eleventy Templating To Include Static Code Demos](https://11ty.rocks/posts/eleventy-templating-static-code-demos/)”:

> This post assumes a foundational familiarity with Eleventy. If you're new to 11ty - welcome! You may want to [start here](https://11ty.rocks/posts/create-your-first-basic-11ty-website/).

Second, [this placard from Robin Sloan](https://society.robinsloan.com/archive/cloud-study/) I unearthed while catching up on newsletters:

> This message was emailed to the Media Lab committee. The assumed audience is subscribers who maintain their own websites or other small apps.

I take these advisories not as infernal warnings to abandon hope, but rather as road signs offered considerately to passersby, each of us at different points along our own journeys.

I hope to see a lot more of them around.

(See also Ethan Marcotte's newly [launched courses on design systems](https://ethanmarcotte.com/wrote/aquent-design-systems-courses/), each with a clearly stated audience: **for Everyone**, **for Developers**, **for Designers**, and **for Product Managers**.)
