---
title: An ode to invisible features
date: 2021-08-06T21:38:28.877Z
category: Development
---
A lot of times we talk about “delightful” user experiences we tend to mean animations or other visual supplements to the interface. But there’s a whole world of nearly invisible features that are, in my opinion, just as enchanting.

Take [GitLab] for example, one of my very favorite tools, and this extremely vanilla, utilitarian view for kicking off a new pipeline (that is, building something from the current Git repo).

<img alt="The GitLab pipeline view showing default branch selected" src="/images/articles/2021-08-ode-invisible-features/main.png">

To run a pipeline for a non-default branch, you click the menu currently showing `main`, search for the branch you need, then click to select it. Simple, straightforward.

But I know from digging around in GitLab docs that `ref` is often used to refer to a given branch in many contexts. What would happen, you might ask yourself, if we tacked `?ref=<branch-name>` onto the URL?

Exactly what you’d hope:

<img alt="The same GitLab pipeline view showing the branch 'lorem/ipsum-dolor' selected" src="/images/articles/2021-08-ode-invisible-features/lorem-ipsum.png">

Maybe this tiny feature exists for reasons other than inquisitive guessers like me, but it opens up so much potential for automation outside the bounds of GitLab itself. Now I can create an [Alfred] workflow to run a new pipeline that accepts a branch name as its argument:

<img alt="User productivity tool Alfred with command 'pipeline lorem/ipsum-dolor' entered" src="/images/articles/2021-08-ode-invisible-features/alfred.png">

Giving me the tools to reduce friction and automate the things I do every single day? _That_ is a textbook case of building in user delight.

[GitLab]: https://gitlab.com
[Alfred]: https://www.alfredapp.com/
