---
title: Responsive Art
date: 2021-12-01T03:43:49.991Z
category: Art
image: "https://multiline.co/images/metatags/2021-11-responsive-art.jpg"
---

Over the holiday weekend I started [playing with an idea](https://twitter.com/ashur/status/1464381126315843588) that’s been bouncing around my head for a little while: could you use CSS Grid to draw “pixels” on a resizable, [responsive](https://piccalil.li/tutorial/create-a-responsive-grid-layout-with-no-media-queries-using-css-grid/) canvas? (“Canvas” as in {% emoji "🎨" %}, not as in <code>&lt;canvas&gt;</code> {% emoji "☺️" %})

Sure! Why not? After all, that’s precisely what CSS Grid is good at (especially when `auto-fill` and `minmax` are in the mix): laying out box-shaped elements on uniform tracks. But I ended up making something I think is far more beautiful and engaging than I imagined, that embraces the nature of CSS; something you might call “responsive art.”

<aside>This post uses interactive figures that may not work well on mobile devices or in an RSS reader. Please consider reading using a desktop browser directly on {{ site.title }}. Thanks!</aside>

### A responsive canvas

Here’s an example of my initial idea — a resizable grid that contains 21 grid elements (“pixels,” for lack of a better term), all with the same dark background color:

<figure class="[][][ canvas-figure ]">
	<div class="[][][ canvas canvas-1 ]">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<figcaption>If you’re using a desktop browser go ahead and grab the lower right corner and resize this canvas to be narrower or wider</figcaption>
</figure>

By using `grid-template-columns: repeat(auto-fill, minmax(...))`, resizing the canvas allows the “pixels” to grow and shrink accordingly. Once the grid can no longer accommodate the current number of elements in a row—or, conversely, if the row width allows for another element to squeeze in while still maintaining the minimum element width—the grid elements reflow into a new layout.

Here’s a snippet of what the relevant CSS looks like:

```css
.canvas {
	--pixel-min: clamp( 30px, 6vmax, 60px );

	display: grid;
	grid-template-columns: repeat(
		auto-fill,
		minmax( var( --pixel-min ), 1fr )
	);
	overflow-x: hidden;
	resize: horizontal;
}

.canvas > * {
	background-color: #000;
	padding-bottom: 100%;
}
```

It’s a little tricky to see what exactly is happening when all the pixels share the same background color, so let’s alternate between light and dark to get a better sense of the layout.

To achieve this we could figure out a way to style each pixel element individually—maybe by creating a `light` or `dark` class and adding it to the appropriate elements, _or_ we could use an `:nth-child` pseudo-class to select every other element with a single CSS rule:

```css
.canvas > *:nth-child(2n) {
	--color: #000;
}
```

<figure class="[][][ canvas-figure ]">
<div class="[][][ canvas canvas-2 ]">
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
	<div></div>
</div>
<figcaption>The same grid figure as above, but with alternating light and dark backgrounds</figcaption>
</figure>

Better! Simple and straightforward to implement, and we can see that the canvas’s elements are reflowing the way we want. (Also, you might notice there’s some fun stuff happening at different widths, but let’s come back to that in just a bit... {% emoji "😉" %})

For some extra flavor, let’s toss a proper palette into the mix using successively greater `:nth-child` values:

```css
.canvas > * {
	--color: #a6035d;
}
.canvas > *:nth-child(2n) {
	--color: #f27405;
}
.canvas > *:nth-child(3n) {
	--color: #d91e41;
}
```

<figure class="[][][ canvas-figure ]">
	<div class="[][][ canvas canvas-3 canvas-colors ]">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<figcaption>A brightly colored canvas with pixels in magenta, orange, and red</figcaption>
</figure>

Now we’re getting somewhere! But even though it’s more colorful it still isn’t particularly _artful_. Adding a bunch of extra pixel elements might get us a little closer to the kind of engaging design I was originally hoping to achieve, but my gut tells me it would just end up a muddy mess of colors.

The `:nth-child` selector approach got me thinking, though: would extending that to other properties beyond just `background-color` produce anything interesting, especially at the intersections of multiple rules?

### Embracing the cascade

With our colors already defined, let’s see what happens as we start adding additional rules to give shape to our pixels.

First, let’s add a rounded corner to the top left corner of every element on the canvas:

```css
.canvas > * {
	border-top-left-radius: 100%;
}
```

<figure class="[][][ canvas-figure ]">
	<div class="[][][ canvas canvas-colors canvas-tl ]">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<figcaption>The same brightly colored canvas, with every pixel rounded at the top left corner</figcaption>
</figure>

Kind of neat, but not much more interesting than the previous iteration of colored squares. Let’s round the top _right_ corner on every other pixel and see what happens:

```css
.canvas > *:nth-child(2n) {
	border-radius: 0;
	border-top-right-radius: 100%;
}
```

<figure class="[][][ canvas-figure ]">
	<div class="[][][ canvas canvas-colors canvas-tl canvas-tr ]">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<figcaption>The same brightly colored canvas, with every second pixel rounded at the top right corner</figcaption>
</figure>

Things are getting a little more interesting! A row of scallops in varying colors is starting to feel more art-y, and depending on the width of the canvas you either get uniform distribution or a nice offset between rows.

(Adding `border-radius: 0` to subsequent `:nth-child` rules to reset and prevent ever-rounder pixels is an aesthetic choice I made after playing around a bit while building this example, but you could absolutely let every previous rule continue to cascade for weirder effects.)

You can probably guess where we’re headed next {% emoji "☺️" %} Let’s round the bottom left corner of every third pixel:

```css
.canvas > *:nth-child(3n) {
	border-radius: 0;
	border-bottom-left-radius: 100%;
}
```

<figure class="[][][ canvas-figure ]">
	<div class="[][][ canvas canvas-colors canvas-tl canvas-tr canvas-bl ]">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<figcaption>The same brightly colored canvas, with every third pixel rounded at the bottom left corner</figcaption>
</figure>

_Wowowow!_ By adding just one more rule, we’ve jumped pretty far outside the strict visual boundaries of our grid to form new shapes woven together by multiple pixels.

Another exciting change with the new rule is that the design starts to change pretty radically at different widths. Try resizing this canvas down to three columns and then up to eleven. They’re obviously all related, but—to me, at least—they evoke very different feelings.

Okay, let’s round out our set by adding a fourth and final rule:

```css
.canvas > *:nth-child(4n) {
	border-radius: 0;
	border-bottom-right-radius: 100%;
}
```

<figure class="[][][ canvas-figure ]">
	<div class="[][][ canvas canvas-colors canvas-tl canvas-tr canvas-bl canvas-br ]">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
	<figcaption>The same brightly colored canvas, with every fourth pixel rounded at the bottom right corner</figcaption>
</figure>

_Voilà!_ With just three rules to set color and four to determine overall shape, we have a beautiful little piece of geometric artwork with _nine_ variations that reveal themselves when you resize the canvas.

(My absolute favorite variation pops out when you shrink this final canvas to five columns {% emoji "🥰" %})

#### Beauty in the grain

One thing I really love about these responsive pieces is the dramatic change in appearance that comes from playing with their dimensions. A single nudge can transform a series of braided, almost organic strands that crisscross the canvas into a rigid repeating pattern with little variation from row to row.

And even though we can fine-tune rules here and there, we’re really letting go of pixel precision and going with the grain of CSS. Moreover, I can’t say I would have thought to put those elements with those shapes in those places using those color distributions; these little canvases truly fall somewhere between handcrafted and generative art.

If you’re interested in playing with more of these, I’ve started to [collect them](https://ashur.cab/rera/projects) over on my personal site. Here are two I’ve published so far:

- [Untitled #1](https://ashur.cab/rera/is/tinkering/responsive-art/)
- [Untitled #2 (Snake)](https://ashur.cab/rera/is/tinkering/responsive-snake/)

(And if you start playing around with this kind of approach or have been already, please [let me know](https://twitter.com/ashur)! I’d absolutely love to see what kind of cool and creative things other people are making with responsive art.)

### Influences

You know when you have fragments and snippets of ideas people have said or written just marinating in your head, then a few disparate thoughts bubble up together at _just_ the right moment? <span role="img" aria-label="chef's kiss">😙👌</span>

[**Miriam Suzanne**](https://twitter.com/TerribleMia) said this about CSS art on the terrific [Word Wrap](https://twitter.com/wordwrapshow/status/1395009344354152449) podcast:

> People that are doing just absolutely weird shit with CSS is so cool. [...] I would love to see more CSS art that plays with resilience. How’s it going to fall apart?

[**Andy Bell**](https://twitter.com/hankchizljaw) echoed Miriam’s idea about resiliency when describing the [responsive grid](https://piccalil.li/tutorial/create-a-responsive-grid-layout-with-no-media-queries-using-css-grid/) that makes the resizable canvas work:

> Embracing the flexible nature of the web gives us resilient user interfaces. Instead of using prescribed, specific sizes, we give elements sensible boundaries and let them auto flow or resize where possible. A good way to think of this is that we provide browsers with some rules and regulations, then let them decide what’s best—using them as a guide.

Finally, [**Amanda Cifaldi**](https://twitter.com/algcifaldi) built and maintains a beautiful art bot on Twitter called [Tiny Spires](https://twitter.com/tinyspires/), whose geometric designs float down my timeline several times a day.

Designing for a grid-based canvas whose dimensions are controlled by the viewer is, in retrospect, very much influenced by all of these.

<style>
	.canvas-figure,
	.canvas-figure * {
		box-sizing: border-box;
	}
	.canvas-figure {
		--pixel-min: clamp( 30px, 6vmax, 60px );
		margin: 0 auto;
		padding-left: var( --pixel-min );
		padding-right: var( --pixel-min );
	}
	.canvas-figure figcaption {
		max-width: 45ch;
	}
	.canvas {
		--shadow-color: 21deg 7% 58%;
		--shadow-elevation-medium:
			-1px 1px 1.6px hsl(var(--shadow-color) / 0.24),
			-2.5px 2.5px 4px -0.6px hsl(var(--shadow-color) / 0.24),
			-4.8px 4.8px 7.6px -1.2px hsl(var(--shadow-color) / 0.24),
			-9.5px 9.5px 15.1px -1.9px hsl(var(--shadow-color) / 0.24),
			-18.3px 18.3px 29.1px -2.5px hsl(var(--shadow-color) / 0.24);

		background-color: var( --canvas-color );
		border: solid #2d3748 calc( var( --pixel-min ) / 4 );
		box-shadow: var( --shadow-elevation-medium );
		display: grid;
		grid-template-columns: repeat(
			auto-fill,
			minmax( var( --pixel-min ), 1fr )
		);
		margin: 0 auto;
		max-width: 100%;
		min-width: calc( var( --pixel-min ) * 5 );
		overflow: hidden;
		padding: calc( var( --pixel-min ) / 2 );
		resize: horizontal;
		width: calc( var( --pixel-min ) * 9 );
	}
	.canvas + * {
		margin-top: 1.5rem;
	}

	.canvas > * {
		padding-bottom: 100%;
		background-color: var( --color );
	}

	.canvas-1 {
		--color: #2d3748;
	}

	.canvas-2 > *:nth-child(2n) {
		--color: #2d3748;
	}

	.canvas-colors > * {
		--color: #a6035d;
	}
	.canvas-colors > *:nth-child(2n) {
		--color: #f27405;
	}
	.canvas-colors > *:nth-child(3n) {
		--color: #d91e41;
	}

	.canvas-tl > *:nth-child(1n) {
		border-top-left-radius: 100%;
	}
	.canvas-tr > *:nth-child(2n) {
		border-radius: 0;
		border-top-right-radius: 100%;
	}
	.canvas-bl > *:nth-child(3n) {
		border-radius: 0;
		border-bottom-left-radius: 100%;
	}
	.canvas-br > *:nth-child(4n) {
		border-radius: 0;
		border-bottom-right-radius: 100%;
	}
</style>
