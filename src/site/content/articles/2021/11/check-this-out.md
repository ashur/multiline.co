---
title: Check This Out
date: 2021-11-11T17:41:38.020Z
category: Projects
image: "https://multiline.co/images/metatags/2021-11-check-this-out.jpg"
---
Public libraries are a genuine treasure.

I'm a big fan of [Libby](https://libbyapp.com/)—an app for Android, iOS, and the web—that lets you borrow e-books and audiobooks from your library.

I found that searching for a book or author in Libby based on a recommendation I ran across online was a little cumbersome for me:

1. Copy (or try and fail to memorize!) the title or author
1. Quit Safari and swipe around trying to remember which screen Libby lives on
1. Launch Libby
1. Hit the Search tab
1. Paste my selection and kick off the search

So, like a good nerd, I made a bookmarklet called [**Check This Out**][cto] that makes it a cinch to start a new book:

1. Select a title or author on any web page
2. Click or tap the _Check This Out_ bookmarklet to launch Libby directly into a search
3. Click or tap one more time to preview, reserve, or check out the book

Since the process of installing a bookmarklet on iOS isn't very straightforward I also made a site to help document the process, and decided to lean _way_ into the vintage paperback book aesthetic:

<picture>
	<source srcset="/images/articles/2021-11-check-this-out/01.avif" type="image/avif">
	<source srcset="/images/articles/2021-11-check-this-out/01.webp" type="image/webp">
	<img src="/images/articles/2021-11-check-this-out/01.png" alt="Check This Out home page, styled to look like a worn vintage paperback novel">
</picture>

<picture>
	<source srcset="/images/articles/2021-11-check-this-out/02.avif" type="image/avif">
	<source srcset="/images/articles/2021-11-check-this-out/02.webp" type="image/webp">
	<img src="/images/articles/2021-11-check-this-out/02.png" alt="Check This Out installation page, styled to look like a worn vintage paperback novel">
</picture>

I'll dive into a few details soon about building the site (the “animated stills” that demonstrate the feature and document the process of installing the bookmarklet were a blast to think through and build).

But for now—if you love to read, check out [**Check This Out**][cto].

[cto]: https://checkthisout.today
