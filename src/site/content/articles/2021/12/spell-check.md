---
title: Spell Check
date: 2021-12-30
category: Projects
image: "https://multiline.co/images/metatags/2021-12-spell-check.jpg"
---
My wife [Emily](https://emily.cab) and I are big fans of the [Spelling Bee](https://nytimes.com/puzzles/spelling-bee), a daily word-finding puzzle from the New York Times.

Since the early days of the pandemic we’ve built a habit of playing together almost every morning. Our routine is to find as many words as we can individually, then over coffee and breakfast we’ll compare words and go for Genius (or, if we’ve gotten there already, try for the elusive [Queen Bee](https://www.nytimes.com/2021/07/26/crosswords/spelling-bee-forum-introduction.html)).

There are days, though, when we need a little assist; a push in the right direction. A plethora of tools and resources already exist to help with this, including the official grid published daily in the Spelling Bee Forum. We’ve had some success with these, but find it a little cumbersome to switch back and forth between the puzzle and the external grid.

So, like a good middle-aged web nerd, I built [Spell Check](https://spellcheck.fun)*, a bookmarklet that fetches the official grid and then adds a progress grid to the game itself:

<picture>
	<source
		srcset="/images/articles/2021-12-spell-check/spell-check.avif"
		type="images/avif"
	/>
	<source
		srcset="/images/articles/2021-12-spell-check/spell-check.webp"
		type="images/webp"
	/>
	<img
		alt="Spell Check being used on a mobile device, showing a grid of remaining words for each letter-length combination."
		src="/images/articles/2021-12-spell-check/spell-check.jpg"
		height="675"
		width="1200"
	/>
</picture>

No more bouncing between sites! Everything is in one place and totals are updated automatically, showing only the number of remaining words. Even better, you don’t have to strain to tell whether a word like ILLICITLY has 7 or 8 letters ever again. (It’s 10, actually. (Or is it 9? {% emoji "😜" %})) Just work your word magic like usual, then do a quick tap-tap to check how close you are to the crown.

If you play the Spelling Bee, I’d be thrilled if you give Spell Check a whirl! Any modern browser will do, on just about any platform. (If you run into issues getting set up or using the bookmarklet, please let me know!)

> *I asked Emily what it should be called, and without missing a beat she said, “Spell Check.” It’s perfect {% emoji "💚" %}
