---
title: Lynn Fisher’s annual refresh
date: 2021-12-16
category: Links
---
It’s time for one of my favorite annual traditions on the web, Lynn Fisher’s [portfolio refresh](https://lynnandtonic.com).

As always, be sure to resize your browser (and zoom out while you’re at it! 👀) Every square pixel is a delight.
