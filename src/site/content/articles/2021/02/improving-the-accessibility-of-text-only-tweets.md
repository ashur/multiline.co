---
title: Improving the Accessibility of Text-Only Tweets
date: 2021-02-02T04:46:27.071Z
category: Accessibility
image: https://multiline.co/images/articles/2021-02-in-black-star-excess-eyebel/alt.png
---
Every few weeks, I see a tweet float down my timeline that goes something like this:

> “This is how your cute tweet, overflowing with scripty text and the typewriter ‘font’ and/or sign bunny, sounds to users who rely on screen readers”

followed by a video of Siri wringing absolutely every syllable out of every Unicode character title:

> “Vur-tih-cul line. Vur-tih-cul line. Back-slash. Un-dur-score. Un-dur-score. Slash. Bu-let…”

It’s genuinely bad! It's also frustrating, impenetrable, and—as these tweets mean to teach or remind us—completely inaccessible if you depend on a screen reader to navigate the internet.

Some admonitions go on, encouraging us to abstain from using text-based illustrations altogether. I think this is well-intentioned—abstinence probably _is_ the best tool at our disposal for the moment—but I have a feeling it's probably not very effective in the long run.

I don't fault the request. People are just trying to ensure we make our gags and jokes accessible, or at least intelligible, to everyone. The trouble is, we humans love to express ourselves in unique and creative ways! (Most _especially_ by copying what everyone else does <span aria-label="shrug" role="img">¯\\\_(ツ)_/¯</span>) Asking people not to use memes, in any medium, feels like a losing battle.

What if—_in addition_ to raising awareness about how Twitter friends currently experience our more visually whimsical tweets—we also encouraged Twitter-the-company to give us tools to make all of our tweets more accessible?

Text- and Unicode-based illustrations are, in essence, proto-images. Imagine if Twitter borrowed from the existing alt-text tooling they already provide. For example, if they detected characters in known ranges like <span aria-label="cute" role="img">𝒸𝓊𝓉ℯ</span>, <span aria-label="gothic" role="img">𝖌𝖔𝖙𝖍𝖎𝖈</span>, [box-drawing characters][bdc], etc., they could automatically display our old friend **Add description** on the tweet composition view, linking to the alt-text definition we know and love:

<img src="/images/articles/2021-02-in-black-star-excess-eyebel/alt.png" alt="Mockup showing 'Add description' button on text-only tweet and alt text editor with description for that tweet">

Consider this simple proof of concept:

<pre aria-label="person peeking out from behind a brick wall: “psst! hey kid, be sure to drink your ovaltine”" role="img" style="margin-bottom: 1.5rem; padding-left: 4rem; font-family: Georgia, Cambria, 'Times New Roman', Times, serif">  ┻┳|
  ┳┻|  psst! hey kid!
  ┻┳|
  ┳┻|
  ┻┳|
  ┳┻|
  ┻┳|
  ┻┳|
  ┳┻|
  ┻┳|
  ┳┻| _
  ┻┳| •.•)  BE SURE
  ┳┻|⊂ﾉ     TO DRINK
  ┻┳|     YOUR
  ┳┻|     OVALTINE
  ┻┳|
</pre>

Wrapped with `aria-label` and `role="img"` attributes, all the characteristics of the original text are preserved—the ability to select, copy, and paste, for one—while assistive technology support is dramatically improved. (Go ahead, take it for a spin!)

No doubt it's a far more complicated affair on a platform like Twitter than what I've cobbled together here, but I'm confident engineers there could give everyday tweeters like you and me the tools and wherewithal to make our non-image illustrations more accessible.

[bdc]: https://en.wikipedia.org/wiki/Box-drawing_character
