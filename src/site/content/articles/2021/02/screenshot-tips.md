---
title: A Few Tips for Capturing Screenshots
date: 2021-02-24T22:41:09.702Z
category: Documentation
image: https://multiline.co/images/metatags/2021-02-screenshot-tips.png
---
The documentation for a [new starter kit][eps] I launched this week featured lots of screenshots stepping readers through configuring two different services. I wanted to make sure each image clearly conveyed a single idea without getting bogged down in extraneous details. These are a few tricks I picked up in the process.

### Use your browser as a camera

I've built a strong muscle memory for quickly grabbing screenshots using the macOS keyboard shortcuts. I typically capture a region of the page and then head over to a simple image editor to tidy up my cropping, align like elements seen in other screenshots, add redaction, etc., until I have the image I'm looking for.

This isn't a bad approach—and sometimes it's the only way to achieve the result you're looking for—but the overhead of running your documentation workflow this way can really start to add up. Instead, we can use tools that are built right into our browsers!

Firefox, Chrome, and Edge include a variety of screenshot options:

- **Visible** — What's currently visible in the viewport, without any browser chrome
- **Full page** — Capture the entire page, including anything that's scrolled out of view
- **Elements** — Hover and click to capture a specific element and its children
- **Selected region** or **Area** — Click and drag to select an arbitrary region for capture

Firefox is my primary browser, so I mostly use the [screenshot tools](https://support.mozilla.org/en-US/kb/firefox-screenshots) in their address bar. Chrome and Edge both offer a similar feature via the [`screenshot` command](https://umaar.com/dev-tips/151-screenshot-capture/) in their DevTools command menus.

### Compose in-camera

Rather than capturing the whole browser window and then switching to an image editor to crop it down to size, sometimes the quickest way to compose the right shot is as simple as resizing your browser and capturing the visible area:

<img src="/images/articles/2021-02-screenshot-tips/compose-in-camera.png" alt="Browser window resized to show only the desired section">

This is similar to capturing a selected region using the system's screenshot tools, with the added benefit that you can reproduce the same frame size for other related screenshots (or retakes of the original <strike>if</strike> when necessary).

### Deemphasize neighboring elements

In some cases, you might want to capture a bit more of the scene to help your readers orient themselves in the environment they'll be navigating while still drawing their attention to a specific element.

One approach is to fire up an image editor or screenshot tool and drop a nice big arrow or other visual indicator pointing to the element in question. This is a great option!

I tried a different idea with this project, and I think the results are pretty nice:

<img src="/images/articles/2021-02-screenshot-tips/deemphasize.png" alt="Netlify dashboard highlighting an action button, with all other text and elements faded out">

This preserves the overall context that a reader will encounter, but focuses on an element that might otherwise get lost in the noise of a complex interface.

Here's what I did:

1. Shrink the viewport to show only what's relevant (i.e., "zoom in" with our browser-camera)
1. Select neighboring elements in the DOM inspector
1. Add a new `opacity` property to each element's style rules
1. Set a value that's appropriate — ex., `0.4` to keep it visible but bring things down to a whisper, or `0` to hide it altogether

This is a bit more involved than simply snapping a screenshot, but as we'll see in a bit it sets us up nicely for potential reshoots.

### Capture just the element you need

Sometimes even the contents of the viewport are still too broad. Rather than shoot the visible area and crop it down, you can use your browser's tools to capture only the element you want to show:

<img src="/images/articles/2021-02-screenshot-tips/capture-element.png" alt="Browser tools highlight a single element for capture">

It works like a charm! With one click, we have a perfectly cropped screenshot with no stray background colors or clipped borders:

<img src="/images/articles/2021-02-screenshot-tips/element.png" alt="The single element as captured">

### Redact with real text

In some cases, you may be documenting an administrative interface like the _Environment variables_ section of Netlify's dashboard. To prevent leaking sensitive information you could redact those details with a big rectangle, pixelated blurring, or some other obfuscation method using your favorite photo editing tool.

Another approach is to use Developer Tools to replace sensitive strings with text that looks similar enough not to be distracting. At a glance, it maintains the overall appearance that your readers will encounter:

<img src="/images/articles/2021-02-screenshot-tips/netlify-admin-env.png" alt="Example showing admin dashboard with fake text that would otherwise reveal sensitive information">

In this case, a snippet of _Lorem ipsum_ set in [Mocking Spongebob](https://knowyourmeme.com/memes/mocking-spongebob) is close enough to the real API key's multi-case alphanumeric format that it doesn't draw attention.

This also lets you bring cohesiveness to otherwise disjointed screenshots. Maybe you switched sample projects halfway through, and now your ID numbers don't match. Don't reshoot the originals if you don't have to! Instead, bring the original project ID _forward_ to your new shots by replacing that value any place it might appear.

### Micro automation

Unfortunately, you can't always avoid retaking screenshots, sometimes multiple times. In those cases, writing and running short JavaScript snippets will help you quickly re-stage your environment.

For example, maybe each of the environment variable value elements above has a class `env-var`. All we'd need to do to swap in our replacement text (yet again!) is run the following snippet in the Developer Tools console:

```
document.querySelectorAll(".env-var")[0]
    .innerText = "lOrEmIpSuMdOlOrSiTaMeTcOnSeCtEtUrAdIpIsCiNgElIt"
```

This lets us skip the DOM diving step altogether!

But what if, as is really the case with the elements above, there isn’t an easily accessible class or ID to use as your selector? All major browsers provide features for copying a selected element's CSS selector path:

1. Open the DOM inspector
1. Right-click the desired element
1. Open the **Copy** menu and choose the appropriate selector menu item

<img src="/images/articles/2021-02-screenshot-tips/select-menus.png" alt="Contextual menus for each of the three major browsers">
<figcaption>Here's a great example of an image that needed some extra tinkering beyond what an in-browser screenshot could provide</figcaption>

Finally, add the otherwise fairly unwieldy selector to your opacity-adjusting snippet:

```
document.querySelector(`#section-environment
    > div:nth-child(2)
    > div:nth-child(1)
    > div:nth-child(3)
    > dl:nth-child(1)
    > dd:nth-child(2)
`).style.opacity = 0.4
```

and you're ready to re-stage that element next time.

### Further Reading

If you're looking for other tips, Melissa McEwen has also been thinking about what makes for good blog post and documentation screenshots, and [shared some great ideas](https://twitter.com/McEwenInt8Array/status/1362830299953037314).

[eps]: https://twitter.com/ashur/status/1363894776630628356
