---
title: Co-op Apps
date: 2021-02-08T19:49:40.234Z
category: Notes
---
I'm intrigued by the growing number of apps and services adopting a co-op model as an alternative to their big, VC-backed counterparts.

Jim Ray is keeping a great running report over at [Flicker Fusion]:

- [Local deliveries](https://flickerfusion.com/2020/12/30/co-ops-not-gigs/)
- [Subscriptions](https://flickerfusion.com/2021/01/27/comradery-subscription-platform/)
- [Video conferencing](https://flickerfusion.com/2021/02/07/meet-coop/)

[Flicker Fusion]: https://flickerfusion.com
