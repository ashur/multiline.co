---
title: Wheels Up
date: 2020-01-29
category: Meta
image: https://images.unsplash.com/uploads/14114640960629b5c3fa0/116dc05a?fit=crop&w=1200&q=80
---

Howdy! My name is Ashur, and my pronouns are he/him/his. This _[gestures unnecessarily]_ is my blog.

My goal here, broadly speaking, is to take the extra legroom afforded by a site of my own to think longer, slower thoughts than I do on Twitter. (They may not be _better_, mind you, but they could hardly be [worse][tw-huge-relief].)


### What to Expect

I love building command-line tools, for work and for fun. (It’s quieter in the terminal, I think, than the buzzing, honking, unending chaos of an internet we’ve clogged with commerce and surveillance.)

I also tend a small stable of [“art” bots][bots] in my spare time. They’re great at making me feel creative, if undeservedly so, and I like the idea they might bring a brief blip of joy to someone’s timeline.

I’ve had a few [false starts][tw-gulp], but a couple of years into futzing around with JavaScript in my spare time, I finally feel like I’m starting to get the [hang of things][tw-npm]. It feels pretty good.

All of which is to say: you can look forward to—or regret—following along as I fumble my way through the ups and downs of programming and re-learning web development.

{% unsplash "uploads/14114640960629b5c3fa0/116dc05a", "Airplane in the clouds", "贝莉儿 DANIST", "https://unsplash.com/@danist07" %}

That’s it, I think? Maybe I’ll [see you around][subscribe] 👋

[bots]: https://ashur.cab/rera/#bots
[tw-huge-relief]: https://twitter.com/ashur/status/1183799540614254592
[tw-gulp]: https://twitter.com/ashur/status/948309634166616064
[tw-npm]: https://twitter.com/ashur/status/1222353208363716608
[subscribe]: /mment/subscribe
