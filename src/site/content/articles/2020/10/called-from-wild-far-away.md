---
title: Called From Wild and Far Away
date: 2020-10-04
category: Music
---
[This single from _Future Islands_][sl] snuck up on me in these early, uneasy days of fall 2020. It reminded me of one of my favorite [little moments][ms]:

> You know when you’re boppin’ through life just minding your own business, and a song pulls up alongside you and matches your speed? Can’t will it to happen, or invite it; can’t go looking for it; just happens. And eventually you look up and over, catch its gaze, and—this is my favorite part—suddenly you can’t remember _not_ knowing the damn thing. It’s part of you now, always has been.
>

I love that.

[sl]: https://futureislands.ffm.to/moonlight
[ms]: https://mastodon.social/@ashur/101231355445736256