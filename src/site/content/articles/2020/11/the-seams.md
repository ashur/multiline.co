---
title: The Seams
date: 2020-11-09T20:48:07.327Z
category: Development
---
My pal Dakota was a glass blower in a past life. I’ve seen a few pieces he made and they’re stunning; elegant but not delicate, like turbulence frozen in time. He really knew his stuff.

We were lollygagging with some other friends a while back and popped into a little curio shop. I caught him eying an ornate piece of glasswork, and asked what he thought. He pointed out a few lines and ridges and, without hesitation, concluded it was not an artisanal creation, as I assumed, but an industrial one.

The marks were tells. They revealed an invisible story of how the piece came to be, one that Dakota could read and I (the sucker, the rube) could not. It was made from a mold, just like its untold identical siblings.

I think of mastering an art (or trade or tool or technology) to that degree—assessing a strange piece of work and seeing not just the thing itself but its constituent parts and how they came together—as learning to see the seams. (Same, too, for the inverse: sizing up a task and, even in the early thrill and  chaos of raw material, seeing a way forward with clarity.)

It's something I'm thinking about as I struggle to transform a new design for this site from sketch to reality. I can't immediately see the underlying HTML and CSS required; how `grid` and `flex` will work together with media queries and `minmax` to bring the complexities of the layout to life.

I don't yet know how to spot the seams.

But this stage I'm in right now—equal parts excitement and frustration, progress and setback—this is the good stuff. These are the heady days of learning.
