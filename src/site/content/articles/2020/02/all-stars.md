---
title: All-Stars
date: 2020-02-20
category: Meta, Development
image: https://images.unsplash.com/photo-1515705576963-95cad62945b6?fit=crop&w=1200&q=80
---

The underlying functionality deployed quietly a couple of weeks ago, but a new featurette I'm calling _All-Stars_ will make its [little debut](/mment/#all-stars) on the home page today, just below this post.

Here's how it came together.

{% unsplash "photo-1515705576963-95cad62945b6", "silhouette of mountains in front of stars", "Denis Degioanni", "https://unsplash.com/@doingenia" %}

### Inspiration

If my math is correct, I've been reading [kottke.org][kottke] for damn near half my life.

When the idea to build **Multiline Comment** started popping around my daydreams, I knew I wanted to borrow a small Kottke-ism straightaway: a _Quick Links_-like interstitial made up of articles and other links I've been reading around the web.

(I don't flatter myself to think the two-penny opera I'm conducting here is comparable to what Jason has built over the last twenty-odd years. My insight doesn't run deep enough to pull off a full-blown link blog, for one. Still, my internal recipe for what constitutes a blog is so deeply seasoned by two decades of following his work that I'm bound to cop a move or two, consciously or otherwise.)

[kottke]: https://kottke.org


### Design & Planning

By virtue of building this site with [Eleventy][11ty], posts like this one are created as [individual Markdown files][gitlab-article] and then extruded through [Nunjucks][njk] templates at build time into blog-shaped HTML. It's a regular [Mop Top Hair Shop][youtube-mths] up in here.

For one hot, stupid minute, I considered using a global [JSON data file][11ty-data-json] for logging and publishing links. (It passed quickly.) Creating and editing text files is perfect for writing whole blog posts, but it's too much overhead for easily sharing a URL here and there, and anyway editing JSON by hand makes me grumpy.

I wanted the process to be easy enough for it to become sticky; realistically, if it was too cumbersome, or required more than a tap or click or two, I'd never get into a good rhythm. Fortunately, I didn't have to look far for a proper solution.

[11ty]: https://11ty.dev
[gitlab-article]: https://gitlab.com/ashur/multiline.co/-/blob/master/src/articles/2020/02/all-stars.md
[njk]: https://mozilla.github.io/nunjucks/
[youtube-mths]: https://www.youtube.com/watch?v=Qs2TfsW_Nc4
[11ty-data-json]: https://11ty.dev/docs/data-global/

#### Made of Star-Stuff

I'll tell anyone who listens how much I love [Feedbin][feedbin], my RSS service of choice. I spend a good portion of my browsing hours checking in there to catch up on blogs, email newsletters ([yup][feedbin-newsletters]!), and other feed-backed goodness.

Like all [good websites][kottke-stellar], Feedbin uses stars for faving. _Read something you like? Hit the star and fave it._ Easy peasy. They also expose an [API][feedbin-api], which includes a query for faved items. If you squint just right, you can see the glimmering of a little link roll {% emoji "✨" %}

The basic functionality, then, should be this: _faving something in Feedbin will make it appear in All-Stars (eventually)._ Let's dig in!

[feedbin]: https://feedbin.com
[kottke-stellar]: https://kottke.org/11/03/introducing-stellar
[feedbin-newsletters]: https://feedbin.com/blog/2016/02/03/subscribe-to-email-newsletters-in-feedbin/
[feedbin-api]: https://github.com/feedbin/feedbin-api


### Fetching Faves

One thing I love about Eleventy is its flexibility. I've tried a number of static-site generators in the past, only to get frustrated by how their visions for a site structure or layout don't match mine. Eleventy is different: everything feels designed to give people a hand building whatever comes to mind. It's a tool not a solution, in the best possible sense.

A particularly nice and powerful feature of Eleventy is its support for [JavaScript data files][11ty-data-js], useful for generating—or fetching!—dynamic content at build time. A script can return static data:

```
module.exports = [
  "user1",
  "user2"
];
```

a function:

```
module.exports = function() {
  return [
    "user1",
    "user2"
  ];
};
```

or even an asynchronous function:

```
module.exports = function() {
  return new Promise((resolve, reject) => {
    resolve([
      "user1",
      "user2"
    ]);
  });
};
```

This is perfect for grabbing data from a live API, munging the results, and handing everything off to the template system for rendering, which is exactly how the back half of _All-Stars_ is implemented.

The API-calling logic lives in [one data file][src-allStars] and doesn't do anything too fancy:

1. Ask Feedbin for all my starred entries
1. Sort them by `id`, largest to smallest (a so-so proxy for "most recently faved")
1. Grab the six most recent entries (a nice, round number for layout: 1&times;6, 2&times;3, or 3&times;2)
1. Clean up the hostname for display purposes (i.e., strip any leading `www.`)
1. Give everything back to Eleventy

That's it!

You'll notice my Feedbin credentials aren't stored in the script or anywhere in the site repository. (I love you but we can't share a login; this isn't Netflix.) Instead, the script looks for two environment variables, `FEEDBIN_EMAIL` and `FEEDBIN_PASSWORD`. During development, they're set in a `.env` file at the root of the project folder. In production, they're defined in Netlify's friendly [deployment dashboard][netlify-env].

[11ty-data-js]: https://11ty.dev/docs/data-js
[src-allStars]: https://gitlab.com/ashur/multiline.co/-/blob/master/src/_data/allStars.js
[netlify-env]: https://docs.netlify.com/configure-builds/environment-variables

#### Fetching Fakes

Adding an external source to my build process introduces a new wrinkle to doing local development.

I use Eleventy's live-reloading `serve` function while tinkering on the project, which means the site is built each time I save my work. This is handy quality-of-life tool, but I'm not always online while I'm working. Even if I were, I don't really want to hit the Feedbin API _every_ single time I make a small change.

I added a quick _Are we in production, or explicitly testing the Feedbin API?_ check at the top of the data function:

```
const {starredEntries} = require( "./dev/allStars-entries" );

// ...

if( process.env.NODE_ENV !== "production" && !process.env.FEEDBIN_LIVE )
{
    console.warn( "Using development data for All-Stars" );
    return Promise.resolve( starredEntries );
}
```
<figcaption><a href="https://gitlab.com/ashur/multiline.co/-/blob/0483d839a133dd3abb9933f88f44a697116b40ce/src/_data/allStars.js#L16">src/_data/allStars.js</a></figcaption>

If not, the function immediately resolves with a set of [fake data objects][src-dev-entries]. Development build times are nice and speedy without a roundtrip to the Feedbin endpoint, and I don't have to worry about whether API requests will resolve.

(P.S. Ask me how I'm using [Nova's][nova] awesome [Build & Run Tasks][nova-build-run] feature to make starting the development server, testing live Feedbin data, and other development details a breeze {% emoji "💫" %})

[src-dev-entries]: https://gitlab.com/ashur/multiline.co/-/blob/master/src/_data/dev/allStars-entries.js
[nova]: https://panic.com/nova
[nova-build-run]: https://www.notion.so/panic/Nova-Run-Tasks-28649a95579d4067accba37ddfa72ef6


### Rendering Faves

I love a good, tidy organization system, and breaking discrete components out into standalone templates really pushes my buttons. The [_All-Stars_ component][src-component] is short and sweet.

One thing that tripped me up for a bit was how to place the component where I wanted it. I've always liked the interstitial layout on the kottke.org home page—the most recent post comes first, followed by _Quick Links_, followed by everything else—and I had hoped to emulate that. I mistakenly looked through the Eleventy documentation at first for a tool to tackle inserting an unrelated template in the middle of a data collection, but this seems to be the purview of templating languages instead.

Nunjucks offers a `loop.index` property, giving the current iteration of a given [`for` loop][njk-for], which we can use to conditionally include the _All-Stars_ template after the second article:

{% raw %}
```
{%- for article in pagination.items -%}
    {% include "article.njk" %}

    {% if loop.index === 1 %}
        {% include "components/all-stars.njk" %}
    {% endif %}
{%- endfor -%}
```
{% endraw %}

<figcaption><a href="https://gitlab.com/ashur/multiline.co/-/blob/0483d839a133dd3abb9933f88f44a697116b40ce/src/_includes/layouts/index.njk#L16">src/_includes/layouts/index.njk</a></figcaption>

Incidentally, this is what has kept <em>All-Stars</em> hidden until now {% emoji "😉" %}

[src-component]: https://gitlab.com/ashur/multiline.co/-/blob/master/src/_includes/components/all-stars.njk
[njk-for]: https://mozilla.github.io/nunjucks/templating.html#for

#### Images

Feedbin's API returns a nice big chunk of metadata about each entry, that looks something like this:

```
{
    "id": 1682191545,
    "feed_id": 1379740,
    "title": "Peter Kafka @pkafka",
    "author": "Peter Kafka",
    "summary": "In 2009, the big magazine publishers built their own digital service so they wouldn't be cut out by Apple or Google. Now they're selling to Apple.",
    "content": "<div>Content</div>",
    "url": "https://twitter.com/fromedome/status/973315765393920000",
    "extracted_content_url": "https://extract.feedbin.com/parser/feedbin/9197b49979d10d5012130f8b456bd5bd040d3206?base64_url=aHR0cDovL3d3dy5jcmFpZ2tlcnN0aWVucy5jb20vMjAxNy8wMy8xMi9nZXR0aW5nLXN0YXJ0ZWQtd2l0aC1qc29uYi1pbi1wb3N0Z3Jlcy8=",
    "published": "2018-03-12T21:52:16.000000Z",
    "created_at": "2018-03-12T22:55:53.437304Z",
    "images": {
        "original_url": "http://www.macdrifter.com/uploads/2018/03/ScreenShot20180312_044129.jpg",
        "size_1": {
            "cdn_url": "https://images.feedbinusercontent.com/85996e1/85996e10ef95a3b96a914e67dfc08d5d3362c6e0.jpg",
            "width": 542,
            "height": 304
        }
    }
}
```

I thought it would be a nice touch to show images whenever they're available, but not every blog post and article has an image associated with it. By pure coincidence, I was working on _All-Stars_ and staring at a smattering of empty gray placeholders when a link from [Waxy.org][waxy] rolled through:

> [**Generative Placeholders**][glitch-gp]<br>
> glitch.me

I had faved it in Feedbin, forgotten(!), and then stumbled across it again at the most opportune moment. As a result, faved entries that dont't bring their own images get a nice, procedurally generated placeholder courtesy of [@fourtonfish][twitter-fourtonfish].

[waxy]: https://waxy.org
[glitch-gp]: https://generative-placeholders.glitch.me
[twitter-fourtonfish]: https://twitter.com/fourtonfish

### Keeping It Fresh

That's the long and short of the _All-Stars_ implementation itself. However, it's mostly for nothing if links are only updated when I push a new change to GitLab and trigger a build in [Netlify][netlify]. _Who cares about a bunch of stale links I faved a few weeks or months ago?_

Netlify gets me off the hook, so to speak, for having to worry about this at all. I generated a new [Build Hook][netlify-build-hook] in the deployment dashboard that builds the `master` branch any time it's called. I added the bog-standard `cURL` command:

```
curl -X POST -d {} https://api.netlify.com/build_hooks/<build_hook_id>
```

to a script on my trusty old DreamObjects droplet, and created a `cron` job to run that script every six hours:

```
0 */6 * * * $HOME/scripts/multiline.co/all-stars.sh
```

That means the random passerby is _at most_ six hours away from finding pipin' hot faves fresh out of the [celestial oven][sagan].

[netlify]: https://netlify.com
[netlify-build-hook]: https://docs.netlify.com/configure-builds/build-hooks/
[sagan]: https://www.seriouseats.com/2010/01/carl-sagans-apple-pie-recipe.html


### Leftovers

I'm pretty happy with how it turned out, but the current implementation leaves a couple of little gaps:

- I can only share from within Feedbin. This felt like enough to get the ball rolling, but I'm working on an enhancement that will let me continue to use Feebin faves _and_ share links I find elsewhere.
- I love the generative placeholders, but Glitch's built-in sleep feature can make things a bit slow on wake. This is probably okay, given the relatively low traffic this little blog gets {% emoji "😊" %}
- The design itself bears more than a passing resemblance to ad dumpsters like Taboola and Outbrain, which is basically mortifying. I'll need to fiddle with things to make it look less skeezy.
- I'm trying to get away from relying on a shared VPS. I haven't found a good `cron`-as-a-Service replacement yet, but if you have one you like please [let me know][twitter]!

[twitter]: https://twitter.com/ashur


### Epilogue

So that's _All-Stars_. Thanks for the inspiration all these years, **kottke.org**. Keep on favin' &#9733;
