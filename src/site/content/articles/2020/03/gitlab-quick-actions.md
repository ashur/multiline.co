---
title: GitLab Quick Actions
date: 2020-03-13
category: Tools
draft: false
---

[GitLab][gitlab] has become an invaluable part of how we do development and testing at [Panic][panic], spanning products, platforms, and disciplines. We use it to host our projects, track issues, organize milestones, and manage merge requests ("pull requests" in the GitHub nomenclature).

Issues and merge requests in particular are constantly in flux: assignment changes, labels come and go, milestones get assigned and reassigned. GitLab's interface is great—things are where you'd expect them—but I'm always looking for ways to reduce friction and automate things I do all day long.

GitLab [Quick Actions][gitlab-qa] are a huge gift to folks like me who feel most productive using a keyboard. Any comment field doubles as a kind of context-specific command line:

![Performing three quick actions on a merge request in GitLab](/images/articles/2020-03-quick-actions.gif)
<figcaption>Throw in a quick <code>Command-Return</code> to submit & execute {% emoji "✨" %}</figcaption>

It's a small trick, but quickly performing a wide array of actions all without leaving the keyboard is a real game changer.

[gitlab]: https://gitlab.com
[panic]: https://panic.com
[gitlab-qa]: https://docs.gitlab.com/ee/user/project/quick_actions.html
