---
title: Innie
date: 2020-08-29
category: Development
image: https://multiline.co/images/metatags/2020-08-innie.png
---
I've been diving in and out of [INI files][wiki] at work a lot lately. One of our projects uses them to manage state in important ways, and some days it feels like all I've done is set or delete the same handful of values, check the results, then do it all over again. (Kind of like that schtick at the optometrist. "One. Two. Back to one. Here comes two again...")

If you're not familiar, INI files contain `key=value` configuration definitions like this:

```ini
city=Portland
street=SW 11th Avenue
zip=97205
```

Sometimes they get _real_ fancy and use sections:

```ini
[weather]
aqi=54
humidity=37
temperature=73
visibility=10
```

Unlike [JSON][json] and [YAML][yaml] files, which explode any time you have the audacity to overlook a trailing comma or rudely insert tabs instead of spaces, INI files aren't a hassle to edit by hand. Still, I found myself wishing for the simplicity and ease of that old Mac command-line standby, [**defaults**][defaults].

So, I wrote [**innie**][npm], a tool for reading, writing, and deleting INI file entries. I even made a little icon, which is pretty ridiculous for something that runs in a terminal:

<img src="/images/articles/2020-08-innie.png" alt="Square with rounded corners. Two white square brackets are oriented to form a capital letter I, with pink-to-purple gradient behind." style="height: 128px; box-shadow: none; margin-left: auto; margin-right: auto;">

Borrowing a page from **defaults**, it offers three subcommands — `read`, `write`, and `delete`. Let's take it for a quick spin:

```shell
$ innie read ./data.ini zip
97205
$ innie write ./data.ini zip 97206
$ innie read ./data.ini zip
97206
$ innie delete ./data.ini zip
$ innie read ./data.ini zip
innie: No such key 'zip'
```

Impressive, huh? {% emoji "😉" "winking face" %}

_"What about those swanky sections we saw earlier?"_ you ask, observant as ever. **innie** supports dot-notation for referencing nested keys:

```shell
$ innie write ./data.ini weather.aqi 58
```

Just incredible.

If you'd like to take a closer look, **innie** is available [on NPM][npm]. You can find instructions for getting set up there or in the Git repo [README][gitlab].

[wiki]: https://en.wikipedia.org/wiki/INI_file
[json]: https://en.wikipedia.org/wiki/JSON
[yaml]: https://en.wikipedia.org/wiki/YAML
[panic]: https://panic.com
[defaults]: https://ss64.com/osx/defaults.html
[npm]: https://www.npmjs.com/package/@aaashur/innie
[npm-ini]: https://www.npmjs.com/package/ini
[npm-dot]: https://www.npmjs.com/package/dot-prop
[gitlab]: https://gitlab.com/ashur/innie
