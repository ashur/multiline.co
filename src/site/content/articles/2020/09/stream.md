---
title: Stream
date: 2020-09-18
category: Apps
---

My Twitter pal [Rob Fahrni][farhni] shipped an iOS app this week, one that’s near and dear to my heart. It’s called [**Stream**][app] and it’s a feed reader for iPhones.

Here’s the pitch:

> [All] your feeds appear as a unified timeline. With its simple user interface, ability to import existing lists, and support for open reading standards, Stream is a great choice if you’re looking for a different experience.

I think he nailed it:

<img src="/images/articles/2020-09-stream-screenshot.png" alt="Stream's timeline and article view" style="margin-left: auto; margin-right: auto;">
<figcaption>Stream’s timeline and article view</figcaption>

Stream is [free on the iOS App Store][app]. (And don’t miss that killer icon — with _six_ variations!)

Congratulations, Rob! You did it {% emoji "🎉" "confetti" %}

[farhni]: https://twitter.com/Fahrni
[app]: https://apps.apple.com/us/app/stream-a-feed-reader/id1377383950
