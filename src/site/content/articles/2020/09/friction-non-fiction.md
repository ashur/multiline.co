---
title: Friction & Non-Fiction
date: 2020-09-13
comment: Buying audiobooks outside Audible and Apple Books isn’t always slick or for the faint of heart, but it feels pretty darn good.
category: Books
image: https://multiline.co/images/metatags/2020-09-friction-non-friction.png
---

One of my all-time favorite radio hosts, [Jacob Goldstein][jg] of [Planet Money][pm], has a new book out. It’s called [_Money: The True Story of a Made-Up Thing_][hb] and in it he tackles the lofty goal of showing us that money is “a useful fiction” we tell ourselves, one that has shaped—and is shaped by—the societies that invented it.

Any longtime Planet Money listener will tell you this is an obvious purchase: Jacob, the funny, insightful, and casually informative co-host of *the* show about money wrote a book *about money*? No-brainer. It was equally obvious to me that I’d opt for the audio version. After years of listening to the show, his voice is instantly familiar and reassuring, two simple things I seek out in _These Tumultuous Times_<sup>&reg;</sup>.

But there’s a wrinkle...

[jg]: https://twitter.com/jacobgoldstein
[pm]: https://www.npr.org/sections/money/
[hb]: https://www.hachettebooks.com/titles/jacob-goldstein/money/9780316417181/

### Big Audio

[Cory Doctorow recently reminded us](https://craphound.com/news/2020/09/08/my-first-ever-kickstarter-the-audiobook-for-attack-surface-the-third-little-brother-book/) that a single behemoth effectively has a stranglehold on the audiobook corner of the publishing world. I’ll spare you any pretense it’s not the exact company you’re thinking of, the one with its mitts around so many other industries today. It’s Amazon, of course, via their Audible subsidiary.

We can’t, any of us, [completely detach ourselves from Amazon](https://nymag.com/intelligencer/2018/03/when-amazon-web-services-goes-down-so-does-a-lot-of-the-web.html), but with a renewed sense of determination not to support them directly and with, uh, *Money* on the line, I decided to explore other options.

As an iOS user, the next obvious choice was Apple Books. I'm no Apple lifer, but even as a relative newcomer to the indie Mac and iOS software development world, I've watched them grow from a seemingly enthusiastic partner into [a hostile landlord](https://twitter.com/dhh/status/1045441782916345856). I can't imagine they're any better to news or book publishers, so it doesn't feel right to tap **Buy** just to save myself some time and a couple of bucks.

Fortunately, there is such a thing a small (indie?) audiobook store. I've noodled around with a few of them in the past, but always get frustrated by small differences between their custom apps and the spoken-word audio player that feels like an old glove: my podcast app of choice, [Castro](https://castro.fm/).

I was paging through the various indie audiobook stores again when it hit me like a name-brand vegetable juice conk to the forehead—Castro! Of course!

### Sideloading

A few years ago, the Supertop fellows rolled out a feature to Castro called [Sideloading](https://blog.supertop.co/post/180053222292/now-playing-in-castro-32-audiobooks). It's a really handy feature I've used a number of times to drop MP3s and other non-podcast audio right alongside episodes of Planet Money and all the other shows I subscribe to. Surely it would work just as well for an audiobook.

I started digging around for a store that sells <abbr title="Digital Rights Management">DRM</abbr>-free copies of their books and, importantly, doesn't tie them up exclusively in their proprietary player. There may be others, but [Downpour](https://www.downpour.com/) caught my eye and seemed to fit the bill. After several taps and some finger-crossing, I was listening to *Money* in Castro.

Because the audio files were zipped into a single bundle, it wasn't _quite_ as straightforward as using the share sheet. It went something like this:

1. Buy [*Money* from Downpour][dp]
2. Open **My Library** and tap the audiobook cover
3. Because they're trying to be helpful, I suspect, Downpour hides the download option on mobile devices. To bring it back, reload the page using Safari's toolbar menu item **Request Desktop Website**
4. Tap the blue **Download** button and then the orange **Download** button
5. When the download completes, launch the **Files** app
6. Locate and tap the zipped audiobook file to uncompress it
7. When that finishes, tap the resulting folder to reveal the individual files
8. *[Wipe brow, take a sip of water]*
9. Select all files and copy or move them to **iCloud Drive** **→ Castro → Sideloads**
10. Launch Castro, and—hey presto!—all 17 tracks are waiting for you in the **Sideloads** section **Library** tab

It is, without a doubt, not nearly as effortless or inexpensive as swallowing my pride yet again and getting another book from Audible, but boy does it feel good.

[dp]: https://www.downpour.com/money?sp=373954