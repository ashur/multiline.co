---
title: Time Machine
date: 2020-05-10
category: Music
---
For the last seven-odd years I’ve kept a running log of the songs I’m listening to at any given time.

It’s a simple routine I started with the late-great Rdio: create a new playlist at the beginning of each month—“May 2015”—and drag songs in as I recognize they’re on regular repeat.

It’s imperfect and lossy; undoubtedly, many songs have fallen through the cracks over the years. Still, I find the ritual of doing it by hand to be soothing, and it’s such a gift to travel through time for just a few minutes.

> I've been seeking out comfort lately, as I'm sure a lot of people can relate, and finding it in a lot of music that makes me think of the past.

— Casey Kolderup, [Amplifier][a]

Especially now.

[a]: https://amplifier.mailchimpsites.com/home
