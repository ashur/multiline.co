/**
 * @param {string} contents
 * @param {string} title
 * @returns {string}
 */
module.exports = (contents, title) =>
{
	// <section class="[][ font:sans max-w border-top margin-bottom:2 margin-top:4 padding:3 ][ article-footer ]">
	return `
	<section class="[][ max-w ][ article-footer ]">
		<div>
			<h3 class="[][][ article-footer-title ]">${ title }</h3>
			${ contents }
		</div>
	</section>`;
}
