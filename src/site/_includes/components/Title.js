const {smartypants} = require( "smartypants" );

/**
 * @param {string} title
 * @param {string} className
 * @return {string}
 */
module.exports = (title, className) =>
{
	title = smartypants( title );
	return `<h2 class="${className}">${title}</h2>`;
};
