const {smartypants} = require( "smartypants" );

/**
 * @param {string} content
 * @return {string}
 */
module.exports = content => smartypants( content );
