/**
 * @param {string} contents
 * @returns {string}
 */
module.exports = (contents, href) =>
{
	return `<a class="[][][ button ]" href="${ href }">${ contents }</a>`;
}
