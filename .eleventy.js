const {DateTime} = require( "luxon" );
const emoji = require( "node-emoji" );
const pluginRss = require( "@11ty/eleventy-plugin-rss" );
const {smartypants} = require( "smartypants" );

module.exports = function( config )
{
	let includeDrafts = false;

	/*
	 * Collections
	 */
	config.addCollection( "publishedArticles", (templateItems) =>
	{
		return templateItems
			.getFilteredByGlob( "src/site/content/articles/**/*.md" )
			.filter( item => !item.data.draft || (item.data.draft && includeDrafts) )
			.sort( (a, b) =>
			{
				return b.date - a.date;
			})
			.map( item =>
			{
				if( !item.data.category )
				{
					item.data.category = "Miscellaneous";
				}

				return item;
			})
	});

	/*
	 * Transforms
	 */

	/*
	 * Filters
	 */
	config.addFilter( "friendlyDate", date =>
	{
		return DateTime.fromJSDate( date, {
	    	zone: 'utc'
	    }).toFormat( 'DDD' );
	});

	config.addFilter( "smartypants", string =>
	{
		if( string )
		{
			return smartypants( string );
		}
	});

	/*
	 * Shortcodes
	 */
	const componentsDir = "./src/site/_includes/components";
	config.addPairedShortcode( "ArticleFooter", require( `${componentsDir}/ArticleFooter` ) );
	config.addPairedShortcode( "Button", require( `${componentsDir}/Button` ) );
	config.addShortcode( "Content", require( `${componentsDir}/Content` ) );
	config.addShortcode( "Title", require( `${componentsDir}/Title` ) );

	config.addShortcode( "emoji", (character, label) =>
	{
		label = label || emoji.unemojify( character )
			.replace( /_/g, " " )
			.replace( /:/g, "" );

		return `<span role="img" aria-label="${label}">${character}</span>`;
	});

	config.addShortcode( "unsplash", (slug, alt, author_name, author_url) =>
	{
		let url = `https://images.unsplash.com/${slug}?fit=crop&w=1800&q=80`;
		return `<img src="${url}" alt="${alt}">\n<figcaption class="credit">Photo by <a href="${author_url}">${author_name}</a></figcaption>`;
	});

	config.addShortcode( "year", () =>
	{
		return new Date().getFullYear();
	});

	/*
	 * Plugins
	 */
	config.addPlugin( pluginRss );

	/*
	 * Miscellaneous
	 */
	config.addPassthroughCopy({ "src/fonts": "fonts" });
	config.addPassthroughCopy({ "src/images": "images" });

	return {
		dir: {
			input: "src/site/content",
			output: "dist",
			data: '../_data',
			includes: '../_includes',
		},

		templateFormats: ["njk", "md"],
	};
};
